'use strict';

// 1. Only render the subtitle (and the p tag) only if the 
// subtitle exists.
// 2. Add a new property in the app. An array of options.
//    Conditionally render a brand new P tag based on items in the 'options' array.
//    If there are no options, render 'No options'.

var app = {
  title: 'Indecision App',
  subtitle: 'Put your life in the hands of a computer!',
  options: ['one', 'two']
};

var template = React.createElement(
  'div',
  null,
  React.createElement(
    'h1',
    null,
    app.title
  ),
  app.subtitle && React.createElement(
    'p',
    null,
    app.subtitle
  ),
  React.createElement(
    'p',
    null,
    app.options && app.options.length ? 'Here are your options' : 'No options'
  ),
  React.createElement(
    'ol',
    null,
    React.createElement(
      'li',
      null,
      'Item One'
    ),
    React.createElement(
      'li',
      null,
      'Item Two'
    )
  )
);

// Setting up user object.
var user = {
  name: 'Bayo',
  age: 33,
  location: 'Brisdane'
};

function getLocation(location) {
  if (location) {
    return React.createElement(
      'p',
      null,
      'Location: ',
      location
    );
  }
}
var templateTwo = React.createElement(
  'div',
  null,
  React.createElement(
    'h1',
    null,
    user.name ? user.name : 'Anonymous user' + '!'
  ),
  (user.age && user.age) >= 18 && React.createElement(
    'p',
    null,
    'Age: ',
    user.age
  ),
  getLocation(user.location),
  React.createElement(
    'h3',
    null,
    'Test'
  )
);

var appRoot = document.getElementById('app');

// Rendering content to the browser.
ReactDOM.render(template, appRoot);
