console.log('The new');

// Vars allows you to redefine a var.
var nameVar = 'Fodeke';
var nameVar = 'Bayo';
console.log('nameVar', nameVar);

let nameLet = 'Adebayo';
nameLet = 'Nicole';
console.log('nameLet', nameLet);

const nameConst = 'Segun';
console.log('nameConst', nameConst);

// Var based variables are function scoped.
// Same goes for let and const.
//  - These are also block level scoped. Code blocks such as if/for.
function getPetName () {
  var petName = 'Hal';
  return petName;
}

// Block scoping. Example with Var.
var fullName = 'Bayo Fodeke';

if (fullName) {
  var firstName = fullName.split(' ')[0];
  console.log('First Name: ', firstName);
}

console.log('Outside IF - First Name: ', firstName);

// Block scoping. Example with const inside the if statment.
var fullName2 = 'Bayo Fodeke';

if (fullName2) {
  const firstName2 = fullName2.split(' ')[0];
  console.log('First Name: ', firstName2);
}

console.log('Outside IF - First Name: ', firstName2);