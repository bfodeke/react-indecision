// 1. Only render the subtitle (and the p tag) only if the 
// subtitle exists.
// 2. Add a new property in the app. An array of options.
//    Conditionally render a brand new P tag based on items in the 'options' array.
//    If there are no options, render 'No options'.

const app = {
  title: 'Indecision App',
  subtitle: 'Put your life in the hands of a computer!',
  options: ['one', 'two']
}

const template = (
  <div>
    <h1>{ app.title }</h1>
    {app.subtitle && <p>{ app.subtitle }</p>}
    <p>{(app.options && app.options.length) ? 'Here are your options' : 'No options'}</p>
    <ol>
      <li>Item One</li>
      <li>Item Two</li>
    </ol>
  </div>
);

// Setting up user object.
const user = {
  name: 'Bayo',
  age: 33,
  location: 'Brisdane'
};

function getLocation(location) {
  if (location) {
    return <p>Location: {location}</p>;
  }
}
const templateTwo = (
  <div>
    <h1>{ user.name ? user.name : 'Anonymous user' + '!' }</h1>
    {(user.age && user.age) >= 18 && <p>Age: { user.age }</p>}
    { getLocation(user.location) }
    { <h3>Test</h3> }
  </div>
);

const appRoot = document.getElementById('app');

// Rendering content to the browser.
ReactDOM.render(template, appRoot);
